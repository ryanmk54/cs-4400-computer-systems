	.file	"q3.c"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	rep ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.globl	switch3
	.type	switch3, @function
switch3:
.LFB1:
	.cfi_startproc
	cmpl	$4, %edx
	ja	.L9
	movl	%edx, %edx
	jmp	*.L5(,%rdx,8)
	.section	.rodata
	.align 8
	.align 4
.L5:
	.quad	.L4
	.quad	.L6
	.quad	.L6
	.quad	.L7
	.quad	.L8
	.text
.L8:
	movl	$27, %eax
	ret
.L4:
	movq	(%rsi), %rax
	movl	(%rdi), %ecx
	movq	%rcx, (%rsi)
	ret
.L6:
	movq	$59, (%rdi)
	movq	(%rsi), %rax
	ret
.L7:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movl	$27, %eax
	ret
.L9:
	movl	$12, %eax
	ret
	.cfi_endproc
.LFE1:
	.size	switch3, .-switch3
	.ident	"GCC: (Ubuntu 4.8.4-2ubuntu1~14.04) 4.8.4"
	.section	.note.GNU-stack,"",@progbits
