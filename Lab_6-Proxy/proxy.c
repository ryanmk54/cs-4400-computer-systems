/**
 * proxy.c - CS:APP Web proxy
 *
 * Listens for requests on a given port.  When a request comes in, 
 * a new HTTP/1.0 request is sent to the server.  The server then replies to
 * the proxy and the proxy forwards the reply onto the client.
 */

#include "csapp.h"
/*
 * Globals
 */
#define PROXY_LOG "proxy.log"  // proxy log file
FILE *log_file; /* Log file with one line per HTTP request */

/*
 * Function prototypes
 */
void *process_request(int connfd, struct sockaddr_in clientaddr);
int parse_uri(char *uri, char *target_addr, char *path, int  *port);
void format_log_entry(char *logstring, struct sockaddr_in *sockaddr, char *uri, int size);
void log_msg(char* msg);

/* Function prototypes copied from Lec23-Sample_Code */
void Getnameinfo(const struct sockaddr *sa, socklen_t salen, char *host,
                 size_t hostlen, char *serv, size_t servlen, int flags);
void gai_error(int code, char *msg); /* Getaddrinfo-style error */
void echo(int connfd);
void unix_warning(char *msg);
void dns_warning(char *msg); /* dns-style error */
ssize_t Rio_readlineb_w(rio_t *rp, void *usrbuf, size_t maxlen);
void Rio_writen_w(int fd, void *usrbuf, size_t n);
int Open_clientfd_w(char *hostname, int port);

/**
 * main - Main routine for the proxy program 
 * Overview on Lec24-slide5
 *
 * Checks program arguments, opens the log file, 
 * and starts listening for requests.
 */
int main(int argc, char **argv)
{
	/* Verify there are 2 arguments */
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <port number>\n", argv[0]);
		exit(0);
	}
	int port = atoi(argv[1]);

	/* Ignore SIGPIPE signals */
	signal(SIGPIPE, SIG_IGN);

	log_file = Fopen(PROXY_LOG, "a"); /* Open the log file in append mode */
	port = atoi(argv[1]); /* parse the port number */

	/* Create a listening descriptor */
    int listenfd, connfd;
    socklen_t clientlen;
    struct sockaddr_in clientaddr;
    char client_hostname[MAXLINE], client_port[MAXLINE];
    listenfd = Open_listenfd(port);

	/* Wait for and process client connections */
	while (1) {
		clientlen = sizeof(struct sockaddr_in); /* Important! */
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
		Getnameinfo((SA *) &clientaddr, clientlen, 
		            client_hostname, MAXLINE, client_port, MAXLINE, 0);
		printf("Connected to (%s, %s)\n", client_hostname, client_port);

		/* sequential call to process_request */
		process_request(connfd, clientaddr);
	}

	exit(0);
}

/*
 * process_request - Executed sequentially or by each thread
 * Overview on Lec24-slide6
 *
 * Read an HTTP request from a client, forward it to the
 * end server (always as a simple HTTP/1.0 request), wait for the
 * response, and then forward back to the client.
 *
 */
void *process_request(int connfd, struct sockaddr_in clientaddr)
{
	int realloc_factor;             /* Used to increase size of request buffer if necessary */
	char *request;                  /* HTTP request from client */
	char *request_uri;              /* Start of URI in first HTTP request header line */
	char *request_uri_end;          /* End of URI in first HTTP request header line */
	char *rest_of_request;          /* Beginning of second HTTP request header line */
	char buf[MAXLINE];              /* General I/O buffer */
	int request_len;                /* Total size of HTTP request */
	int n;                          /* General index and counting variables */
	rio_t  rio;

	/* 
	 * Read the entire HTTP request into the request buffer, one line
	 * at a time.
	 */
	request = (char *)Malloc(MAXLINE);
	request[0] = '\0';
	realloc_factor = 2;
	request_len = 0;
	Rio_readinitb(&rio, connfd);
	while (1) {
		if ((n = Rio_readlineb_w(&rio, buf, MAXLINE)) <= 0) {
			printf("process_request: client issued a bad request (1).\n");
			close(connfd);
			free(request);
			return NULL;
		}

		/* If not enough room in request buffer, make more room */
		if (request_len + n + 1 > MAXLINE)
			Realloc(request, MAXLINE*realloc_factor++);

		strcat(request, buf);
		request_len += n;

		/* An HTTP requests is always terminated by a blank line */
		if (strcmp(buf, "\r\n") == 0){
			break;
		}
	}
		
	/* 
	 * Make sure that this is indeed a GET request
	 */
	if (strncmp(request, "GET ", strlen("GET "))) {
		printf("process_request: Received non-GET request\n");
		close(connfd);
		free(request);
		return NULL;
	}
	request_uri = request + 4;

	/*
	 * Extract the URI from the request
	 */
	int i;
	request_uri_end = NULL;
	for (i = 0; i < request_len; i++) {
		if (request_uri[i] == ' ') {
			request_uri[i] = '\0';
			request_uri_end = &request_uri[i];
			break;
		}
	}

	/* 
	 * If we hit the end of the request without encountering a
	 * terminating blank, then there is something screwy with the
	 * request
	 */
	if ( i == request_len ) {
		printf("process_request: Couldn't find the end of the URI\n");
		close(connfd);
		free(request);
		return NULL;
	}

	/*
	 * Make sure that the HTTP version field follows the URI 
	 */
	if (strncmp(request_uri_end + 1, "HTTP/1.0\r\n", strlen("HTTP/1.0\r\n")) &&
		strncmp(request_uri_end + 1, "HTTP/1.1\r\n", strlen("HTTP/1.1\r\n"))) {
		printf("process_request: client issued a bad request (4).\n");
		close(connfd);
		free(request);
		return NULL;
	}

	/*
	 * We'll be forwarding the remaining lines in the request
	 * to the end server without modification
	 */
	rest_of_request = request_uri_end + strlen("HTTP/1.0\r\n") + 1;

	/* Call parse_uri, URI is in request_uri, to be used as input to parse_uri*/
	char* hostname = Malloc(sizeof(char)*MAXLINE);
	char* pathname = Malloc(sizeof(char)*MAXLINE);
	int port = 0;
	parse_uri(request_uri, hostname, pathname, &port);

	/* Forward request to the end server using open_clientfd */
	int send_to_fd = Open_clientfd_w(hostname, port);

	/* If we are unable to connect to the host, close the client connection */
	if(send_to_fd < 0){
		Close(connfd);
		return NULL;
	}

	/* Create the request to send */
	char* request_to_send = Malloc(sizeof(char)*MAXLINE);
	sprintf(request_to_send, "GET /%s HTTP/1.0 \nHOST: %s \n%s\r\n", pathname, hostname, rest_of_request);

	/* Send the request to the server */
	rio_t send_to_rio;
	Rio_readinitb(&send_to_rio, send_to_fd);
	Rio_writen_w(send_to_fd, request_to_send, strlen(request_to_send));

	/* Send the response back to the client */
	int bytes_sent = 0;
	char* response_buf = Malloc(sizeof(char)*MAXLINE);
	/* while loop reads from server and writes to client, see Echo, Fig. 11.22 */
	while((n = Rio_readlineb_w(&send_to_rio, response_buf, MAXLINE)) != 0) {
		bytes_sent += n;
		Rio_writen_w(connfd, response_buf, strlen(response_buf));
	}

	/* Log the request to disk */
	char* logstring = Malloc(sizeof(char)*MAXLINE);
	format_log_entry(logstring, &clientaddr, request_uri, bytes_sent);
	log_msg(logstring);

	/* Clean up to avoid memory leaks, close fds */
	Close(send_to_fd); 
	Close(connfd);

	return NULL;
}

/*
 * parse_uri - URI parser
 *
 * Given a URI from an HTTP proxy GET request (i.e., a URL), extract
 * the host name, path name, and port.  The memory for hostname and
 * pathname must already be allocated and should be at least MAXLINE
 * bytes. Return -1 if there are any problems.
 */
int parse_uri(char *uri, char *hostname, char *pathname, int *port)
{
	char *hostbegin;
	char *hostend;
	char *pathbegin;
	int len;

	if (strncasecmp(uri, "http://", 7) != 0) {
		hostname[0] = '\0';
		return -1;
	}

	/* Extract the host name */
	hostbegin = uri + 7;
	hostend = strpbrk(hostbegin, " :/\r\n\0");
	len = hostend - hostbegin;
	strncpy(hostname, hostbegin, len);
	hostname[len] = '\0';

	/* Extract the port number */
	*port = 80; /* default */
	if (*hostend == ':')   
		*port = atoi(hostend + 1);

	/* Extract the path */
	pathbegin = strchr(hostbegin, '/');
	if (pathbegin == NULL) {
		pathname[0] = '\0';
	}
	else {
		pathbegin++;	
		strcpy(pathname, pathbegin);
	}
	return 0;
}

/*
 * format_log_entry - Create a formatted log entry in logstring.
 *
 * The inputs are the socket address of the requesting client
 * (sockaddr), the URI from the request (uri), and the size in bytes
 * of the response from the server (size).
 */
void format_log_entry(char *logstring, struct sockaddr_in *sockaddr,
		char *uri, int size)
{
	time_t now;
	char time_str[MAXLINE];
	unsigned long host;
	unsigned char a, b, c, d;

	/* Get a formatted time string */
	now = time(NULL);
	strftime(time_str, MAXLINE, "%a %d %b %Y %H:%M:%S %Z", localtime(&now));

	/*
	 * Convert the IP address in network byte order to dotted decimal
	 * form. Note that we could have used inet_ntoa, but chose not to
	 * because inet_ntoa is a Class 3 thread unsafe function that
	 * returns a pointer to a static variable (Ch 13, CS:APP).
	 */
	host = ntohl(sockaddr->sin_addr.s_addr);
	a = host >> 24;
	b = (host >> 16) & 0xff;
	c = (host >> 8) & 0xff;
	d = host & 0xff;


	/* Return the formatted log entry string */
	sprintf(logstring, "%s: %d.%d.%d.%d %s %d\n", time_str, a, b, c, d, uri, size);
}

/** 
 * appends the message to the log file
 */
void log_msg(char* msg){
	fprintf(log_file, "%s", msg);
	fflush(log_file);
}


/* Functions copied from csapp.c from Lec23-Sample_Code */
void Getnameinfo(const struct sockaddr *sa, socklen_t salen, char *host, 
                 size_t hostlen, char *serv, size_t servlen, int flags)
{
    int rc;

    if ((rc = getnameinfo(sa, salen, host, hostlen, serv, 
                          servlen, flags)) != 0) 
        gai_error(rc, "Getnameinfo error");
}


void gai_error(int code, char *msg) /* Getaddrinfo-style error */
{
    fprintf(stderr, "%s: %s\n", msg, gai_strerror(code));
    exit(0);
}


void echo(int connfd)
{
	size_t n;
	char buf[MAXLINE];
	rio_t rio;

	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb_w(&rio, buf, MAXLINE)) != 0) { 
		printf("server received %d bytes\n", (int)n);
		Rio_writen_w(connfd, buf, n);
	}
}


ssize_t Rio_readlineb_w(rio_t *rp, void *usrbuf, size_t maxlen) 
{
	ssize_t rc;

	if ((rc = rio_readlineb(rp, usrbuf, maxlen)) < 0){
		unix_warning("Rio_readlineb_w error");
	}
	return rc;
} 


int Open_clientfd_w(char *hostname, int port) 
{
	int rc;

	if ((rc = open_clientfd(hostname, port)) < 0) {
		if (rc == -1)
			unix_warning("Open_clientfd_w Unix warning");
		else
			dns_warning("Open_clientfd_w DNS warning");
	}
	return rc;
}


void unix_warning(char *msg) /* unix-style warning */
{
	fprintf(stderr, "%s: %s\n", msg, strerror(errno));
}


void dns_warning(char *msg) /* dns-style error */
{
    fprintf(stderr, "%s: DNS warning %d\n", msg, h_errno);
}


void Rio_writen_w(int fd, void *usrbuf, size_t n) 
{
	if (rio_writen(fd, usrbuf, n) != n)
		unix_warning("Rio_writen error");
}
