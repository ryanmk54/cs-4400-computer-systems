#include <stdio.h>
#include <limits.h>

void main(){
  unsigned short unsignedMax = USHRT_MAX;
  signed short signedMax =  SHRT_MAX;
  short x = 26789;
  int signedY = signedMax - x;
  int unsignedY = unsignedMax - x;
  printf("unsignedMax: %d[%x]\n", unsignedMax, unsignedMax);
  printf("signedMax: %d[%x]\n", signedMax, signedMax);
  printf("unsignedY: %d[%x]\n", unsignedY, unsignedY);
  printf("signedY: %d[%x]\n", signedY, signedY);
}

