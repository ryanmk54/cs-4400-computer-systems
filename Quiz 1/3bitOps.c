#include <stdio.h>

/* Tests for Quiz 1 #3 */
void main(){
  unsigned int h1=0xabc;
  unsigned int h2=0x123;
  int andOp = h1 & h2;
  int orOp = h1 | h2;
  int xorOp = h1 ^ h2;
  int logicalAndOp = !h1&&h2;
  printf("andOp: %d[%x]\n", andOp, andOp);
  printf("orOp: %d[%x]\n", orOp, orOp);
  printf("xorOp: %d[%x]\n", xorOp, xorOp);
  printf("logicalAndOp: %d[%x]\n", logicalAndOp, logicalAndOp);
}
