int main(){
  printf("The program is running\n");
  int x = 1;
  int r = 2*x;
  int c = x/r;
  int anotherVal = someFn(x, r);
  printf("The program has finished\n");
  return anotherVal;
}

int someFn(int x, int y){
  x = 2*x;
  y = 5*y;
  x = x + 10;
  while( y > 0){
    x = x >> 1;
    x *= 2;
    printf("In the loop\n");
  }
  printf("I am in someFn\n");
  y = y + 50;
  return x + y;
}

