/* 
 * CS:APP Data Lab 
 * 
 * <Please put your name and userid here>
 * 
 * bits.c - Source file with your solutions to the Lab.
 *          This is the file you will hand in to your instructor.
 *
 * WARNING: Do not include the <stdio.h> header; it confuses the dlc
 * compiler. You can still use printf for debugging without including
 * <stdio.h>, although you might get a compiler warning. In general,
 * it's not good practice to ignore compiler warnings, but in this
 * case it's OK.  
 */

#if 0
/*
 * Instructions to Students:
 *
 * STEP 1: Read the following instructions carefully.
 */

You will provide your solution to the Data Lab by
editing the collection of functions in this source file.

INTEGER CODING RULES:
 
  Replace the "return" statement in each function with one
  or more lines of C code that implements the function. Your code 
  must conform to the following style:
 
  int Funct(arg1, arg2, ...) {
      /* brief description of how your implementation works */
      int var1 = Expr1;
      ...
      int varM = ExprM;

      varJ = ExprJ;
      ...
      varN = ExprN;
      return ExprR;
  }

  Each "Expr" is an expression using ONLY the following:
  1. Integer constants 0 through 255 (0xFF), inclusive. You are
      not allowed to use big constants such as 0xffffffff.
  2. Function arguments and local variables (no global variables).
  3. Unary integer operations ! ~
  4. Binary integer operations & ^ | + << >>
    
  Some of the problems restrict the set of allowed operators even further.
  Each "Expr" may consist of multiple operators. You are not restricted to
  one operator per line.

  You are expressly forbidden to:
  1. Use any control constructs such as if, do, while, for, switch, etc.
  2. Define or use any macros.
  3. Define any additional functions in this file.
  4. Call any functions.
  5. Use any other operations, such as &&, ||, -, or ?:
  6. Use any form of casting.
  7. Use any data type other than int.  This implies that you
     cannot use arrays, structs, or unions.

 
  You may assume that your machine:
  1. Uses 2s complement, 32-bit representations of integers.
  2. Performs right shifts arithmetically.
  3. Has unpredictable behavior when shifting an integer by more
     than the word size.

EXAMPLES OF ACCEPTABLE CODING STYLE:
  /*
   * pow2plus1 - returns 2^x + 1, where 0 <= x <= 31
   */
  int pow2plus1(int x) {
     /* exploit ability of shifts to compute powers of 2 */
     return (1 << x) + 1;
  }

  /*
   * pow2plus4 - returns 2^x + 4, where 0 <= x <= 31
   */
  int pow2plus4(int x) {
     /* exploit ability of shifts to compute powers of 2 */
     int result = (1 << x);
     result += 4;
     return result;
  }

FLOATING POINT CODING RULES

For the problems that require you to implent floating-point operations,
the coding rules are less strict.  You are allowed to use looping and
conditional control.  You are allowed to use both ints and unsigneds.
You can use arbitrary integer and unsigned constants.

You are expressly forbidden to:
  1. Define or use any macros.
  2. Define any additional functions in this file.
  3. Call any functions.
  4. Use any form of casting.
  5. Use any data type other than int or unsigned.  This means that you
     cannot use arrays, structs, or unions.
  6. Use any floating point data types, operations, or constants.


NOTES:
  1. Use the dlc (data lab checker) compiler (described in the handout) to 
     check the legality of your solutions.
  2. Each function has a maximum number of operators (! ~ & ^ | + << >>)
     that you are allowed to use for your implementation of the function. 
     The max operator count is checked by dlc. Note that '=' is not 
     counted; you may use as many of these as you want without penalty.
  3. Use the btest test harness to check your functions for correctness.
  4. Use the BDD checker to formally verify your functions
  5. The maximum number of ops for each function is given in the
     header comment for each function. If there are any inconsistencies 
     between the maximum ops in the writeup and in this file, consider
     this file the authoritative source.

/*
 * STEP 2: Modify the following functions according the coding rules.
 * 
 *   IMPORTANT. TO AVOID GRADING SURPRISES:
 *   1. Use the dlc compiler to check that your solutions conform
 *      to the coding rules.
 *   2. Use the BDD checker to formally verify that your solutions produce 
 *      the correct answers.
 */


#endif
/* 
 * bitAnd - x&y using only ~ and | 
 *   Example: bitAnd(6, 5) = 4
 *   Legal ops: ~ |
 *   Max ops: 8
 *   Rating: 1
 */
int bitAnd(int x, int y) {
  /* I figured this out by messing around with truth tables.
     Then I realized this is De Morgan's law.
     If you punch the outside ~ into the (),
     the inside nots go away and the | turns into a & */
  return ~(~x | ~y);
}
/* 
 * getByte - Extract byte n from word x
 *   Bytes numbered from 0 (LSB) to 3 (MSB)
 *   Examples: getByte(0x12345678,1) = 0x56
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 6
 *   Rating: 2
 */
int getByte(int x, int n) {
  /* Shift x right by 8 times n.
   * 8*n is n shifted left 3 b/c 8 is 2^3
   */
  int xShift = n << 3;
  int byte = x >> xShift;
  return byte & 0xFF;
}
/* 
 * logicalShift - shift x to the right by n, using a logical shift
 *   Can assume that 0 <= n <= 31
 *   Examples: logicalShift(0x87654321,4) = 0x08765432
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 20
 *   Rating: 3 
 */
int logicalShift(int x, int n) {
  /* Create a 1 mask if n > 0.
     Shift it all the way to the left to mask starting from the left.
     Make the mask as big as the shift.
     Shift x n times.
     Mask x to get rid of the 1's that came from the arithmetic shift
  */
  int mask = !!n;
  int negOne = ~1 + 1;
  mask = mask << 31;
  mask = mask >> (n + negOne);
  mask = ~mask;
  x = x >> n;
  x = x & mask;
  return x;
}
/*
 * bitCount - returns count of number of 1's in word
 *   Examples: bitCount(5) = 2, bitCount(7) = 3
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 40
 *   Rating: 4
 */
int bitCount(int x) {
  /* Split x into groups of four.
   * Mask everything except the first bit of every group
   * Get the first, second, third, ... bits of each group
   * by shifting by 1 and then masking.  
   * Add up the bits in each part, then add up all the parts
   */
  int mask = 0x01;
  int firstBits = 0x0;
  int secondBits = 0x0;
  int thirdBits = 0x0;
  int fourthBits = 0x0;
  int fifthBits = 0x0;
  int sixthBits = 0x0;
  int seventhBits = 0x0;
  int eigthBits = 0x0;
  int totalBitsInParts = 0x0;
  int totalBits = 0x0;

  mask += mask << 8;
  mask += mask << 16;

  firstBits = x & mask;
  secondBits = (x >> 1) & mask;
  thirdBits = (x >> 2) & mask;
  fourthBits = (x >> 3) & mask;
  fifthBits = (x >> 4) & mask;
  sixthBits = (x >> 5) & mask;
  seventhBits = (x >> 6) & mask;
  eigthBits = (x >> 7) & mask;
  totalBitsInParts = firstBits + secondBits + thirdBits + fourthBits +
                     fifthBits + sixthBits + seventhBits + eigthBits;
  totalBits = (totalBitsInParts & 0xff) + 
              ((totalBitsInParts >> 8) & 0xff) +
              ((totalBitsInParts >> 16) & 0xff) + 
              ((totalBitsInParts >> 24) & 0xff); 
  return totalBits;
}

/* 
 * bang - Compute !x without using !
 *   Examples: bang(3) = 0, bang(0) = 1
 *   Legal ops: ~ & ^ | + << >>
 *   Max ops: 12
 *   Rating: 4 
 */
int bang(int x) {
  /* 1. '|' every bit with the first bit
   *    so that if any of them are one, the first one will be also.
   * 2. '~' the number to flip the first bit 
   * 3. 'and' the number with 1 so that all bits go away except the first
   * return the number
   */
  int lastOneStanding = x;
  int prepareToOr = lastOneStanding >> 16;
  lastOneStanding = lastOneStanding | prepareToOr;
  prepareToOr = lastOneStanding >> 8;
  lastOneStanding = lastOneStanding | prepareToOr;
  prepareToOr = lastOneStanding >> 4;
  lastOneStanding = lastOneStanding | prepareToOr;
  prepareToOr = lastOneStanding >> 2;
  lastOneStanding = lastOneStanding | prepareToOr;
  prepareToOr = lastOneStanding >> 1;
  lastOneStanding = lastOneStanding | prepareToOr;
  lastOneStanding = ~lastOneStanding;
  lastOneStanding = lastOneStanding & 0x1;
  return lastOneStanding;
}
/* 
 * tmin - return minimum two's complement integer 
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 4
 *   Rating: 1
 */
int tmin(void) {
  return 0x8 << 28;
}
/* 
 * fitsBits - return 1 if x can be represented as an 
 *  n-bit, two's complement integer.
 *   1 <= n <= 32
 *   Examples: fitsBits(5,3) = 0, fitsBits(-4,3) = 1
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 15
 *   Rating: 2
 */
int fitsBits(int x, int n) {
  /* All bits in x in position n or greater need to be the same as the sign bit.
   * 1. Mask off all bits in position less than n.
   * 3. Right shift x by n.
   *    This makes so every bit in x is either the sign bit or in the position 
   *    n or greater
   * 4. To check if every bit is the same as the sign bit, we can check if every
   *    bit is the same.  
   *    To check if x is all zeros:
   *        !(or every bit) this will tell us if there are not any ones 
   *    To check if x is all ones:
   *        (and every bit and mask it with 0x1)
   * 5. x fits in n bits if it is allZeros or allOnes
   */
  int negOne = ~1 + 1;
  int extensionBits = (x & (~0x0 << (n+negOne))) >> (n + negOne);
  int allZeros = !extensionBits; 
  int allOnes = !~extensionBits;
  return allZeros | allOnes;
}

/* 
 * divpwr2 - Compute x/(2^n), for 0 <= n <= 30
 *  Round toward zero
 *   Examples: divpwr2(15,1) = 7, divpwr2(-33,4) = -2
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 15
 *   Rating: 2
 */
int divpwr2(int x, int n) {
    /* Formula from the book on page 106
     * x/(2^n) == x >> n.
     * For negative numbers, add in a bias that will round towards zero
     */
    int sign = x >> 31;
    int negOne = ~1 + 1;
    int bias = (1<<n) + negOne;
    int biasOnlyNeg = bias & sign;
    return (x + biasOnlyNeg) >> n;
}
/* 
 * negate - return -x 
 *   Example: negate(1) = -1.
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 5
 *   Rating: 2
 */
int negate(int x) {
  /* Negating a twos complement integer
   * is the same as flipping the bits and adding 1 
   */
  return ~x + 1;
}
/* 
 * isPositive - return 1 if x > 0, return 0 otherwise 
 *   Example: isPositive(-1) = 0.
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 8
 *   Rating: 3
 */
int isPositive(int x) {
  /* Extract the sign bit by shifting 31 to right
   * Check to see if it is not zero by banging it twice
   * It is greater than zero it is not negative and not zero
   */
  int negative = x >> 31;
  int notZero = !!x;
  int greaterThanZero = (!negative) & notZero;
  return greaterThanZero;
}
/* 
 * isLessOrEqual - if x <= y  then return 1, else return 0 
 *   Example: isLessOrEqual(4,5) = 1.
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 24
 *   Rating: 3
 */
int isLessOrEqual(int x, int y) {
  /* I'll tackle this like a tree with a lot of cases
   * y is either positive or negative
   * If y is positive
   *    x is either positive or negative
   *    if x is negative
   *      y is greater than x, return 1
   *    if x is positive
   *      take the difference
   *      if the difference is greater than or equal to 0
   *      y is greater than or equal to x, return 1
   *      else return 0
   * If y is negative
   *    if x is negative
   *       take the difference
   *       if the difference is greater than or equal to 0
   *       y is greater than or equal to x, return 1
   *       else return 0
   */ 
  int xSign = x >> 31;
  int xNegative = xSign ^ 0;
  int xPositive = !xNegative;
  int ySign = y >> 31;
  int yNegative = ySign ^ 0;
  int yPositive = !yNegative;
  int negativeX = ~x + 1;
  int difference = y + negativeX;
  int differenceSign = difference >> 31;
  int diffGTEQzero = !differenceSign;
  
  return (yPositive & xNegative) | 
         (yPositive & xPositive & diffGTEQzero) |
         (yNegative & xNegative & diffGTEQzero);
}
/*
 * ilog2 - return floor(log base 2 of x), where x > 0
 *   Example: ilog2(16) = 4
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 90
 *   Rating: 4
 */
int ilog2(int x) {
  /* Or the leftmost 1 with all the ones to the right and then bitcount.
   * This works because the log base 2 is the same as the position of the most
   * significant 1.  The most significant 1 is the bitcount minus 1.
   */
  int negOne = ~1 + 1;
  int last2Bits = x | (x >> 1);
  int last4Bits = last2Bits | (last2Bits >> 2);
  int last8Bits = last4Bits | (last4Bits>> 4);
  int last16Bits = last8Bits | (last8Bits >> 8);
  int allBits = last16Bits | (last16Bits >> 16);

  int mask = 0x01;
  int firstBits = 0x0;
  int secondBits = 0x0;
  int thirdBits = 0x0;
  int fourthBits = 0x0;
  int fifthBits = 0x0;
  int sixthBits = 0x0;
  int seventhBits = 0x0;
  int eigthBits = 0x0;
  int totalBitsInParts = 0x0;
  int totalBits = 0x0;

  mask += mask << 8;
  mask += mask << 16;

  firstBits = allBits & mask;
  secondBits = (allBits >> 1) & mask;
  thirdBits = (allBits >> 2) & mask;
  fourthBits = (allBits >> 3) & mask;
  fifthBits = (allBits >> 4) & mask;
  sixthBits = (allBits >> 5) & mask;
  seventhBits = (allBits >> 6) & mask;
  eigthBits = (allBits >> 7) & mask;
  totalBitsInParts = firstBits + secondBits + thirdBits + fourthBits +
                     fifthBits + sixthBits + seventhBits + eigthBits;
  totalBits = (totalBitsInParts & 0xff) + 
                  ((totalBitsInParts >> 8) & 0xff) +
                  ((totalBitsInParts >> 16) & 0xff) + 
                  ((totalBitsInParts >> 24) & 0xff); 
  return totalBits + negOne;
}
/* 
 * float_neg - Return bit-level equivalent of expression -f for
 *   floating point argument f.
 *   Both the argument and result are passed as unsigned int's, but
 *   they are to be interpreted as the bit-level representations of
 *   single-precision floating point values.
 *   When argument is NaN, return argument.
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 10
 *   Rating: 2
 */
unsigned float_neg(unsigned uf) {
  /* 1. Separate out the signBit, exponent, and significand 
   *    for the special numbers.
   * 2. Check for NaN
   * 3. Flip the sign bit
   */
  int exponentMask = 0xFF << 23;
  int exponent = uf & exponentMask;
  int signBitMask = (0x1 << 31);
  int significandMask = ~(signBitMask + exponentMask);
  int significand = uf & significandMask;

  if(exponent == exponentMask)
    if(significand) 
      return uf;

  return uf + signBitMask;
}
/* 
 * float_i2f - Return bit-level equivalent of expression (float) x
 *   Result is returned as unsigned int, but
 *   it is to be interpreted as the bit-level representation of a
 *   single-precision floating point values.
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 30
 *   Rating: 4
 */
unsigned float_i2f(int x) {
  /* A float is split into 3 parts:
   * the sign, exponent, and mantissa.
   * To get the sign:
   *    1. shift left by 31
   *    2. mask it with 1
   * To get the exponent: 
   *    1. count the position of the most significant 1 
   *       in the positive version of x.
   * To get the mantissa:
   *    1. Subtract a 1 from the positive version of x where it would be 
   *       chopped off
   *    2. Use the round to nearest method to add:
   *       1. Get the nearest value higher than the original
   *       2. Get the nearest value lower than the original
   *       3. Use the value that is closer to the original
   *       4. If they are equally close, use the even one.
   * After getting the 3 parts of the float, shift them to their proper places
   * and add them together
   */
  int allOnes = ~0;
  int signPart = x & (allOnes << 31);
  int positiveX = signPart? ~x + 1: x;
  int positiveChangingX = positiveX;
  int exponent = -1;
  int mantissa;
  int mantissaPart;
  int exponentPart;

  // Special Cases
  if(x == 0)
    return 0;

  while(positiveChangingX){
    if(exponent == 30)
      break;
    positiveChangingX >>= 1;
    exponent++;
  }

  // mantissa needs to be rounded to nearest if anything will get cutoff
  mantissa = positiveX - (1 << exponent);
  if(exponent > 23 ){
    int shiftAmount = exponent - 23;
    int cutOffMask = allOnes << shiftAmount ;
    int cutOffAddOne = 0x1 << shiftAmount;

    int lowerValue = mantissa & cutOffMask;
    int higherValue = lowerValue + cutOffAddOne;
    int origSubLow = mantissa - lowerValue;
    int origSubHigh = higherValue - mantissa;

    if(origSubLow < origSubHigh){
      mantissa = lowerValue;
    }
    else if(origSubHigh < origSubLow){
      mantissa  = higherValue;
    }
    else{
      if(lowerValue & cutOffAddOne)
        mantissa = higherValue;
      else
        mantissa = lowerValue;
    }
    mantissaPart = mantissa >> (shiftAmount);
  }
  else{
    int shiftAmount = 23-exponent;
    mantissaPart = mantissa << (shiftAmount);
  }

  exponentPart = (exponent + 127) << 23;
  return signPart + exponentPart + mantissaPart;
}
/* 
 * float_twice - Return bit-level equivalent of expression 2*f for
 *   floating point argument f.
 *   Both the argument and result are passed as unsigned int's, but
 *   they are to be interpreted as the bit-level representation of
 *   single-precision floating point values.
 *   When argument is NaN, return argument
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 30
 *   Rating: 4
 */
unsigned float_twice(unsigned uf) {
  /* Cases:
   *    uf is 0:  my code doesn't work when uf is 0 
   *              so just make this a special case
   *    exp is all 1s:  this is either +/- inf or NaN, so don't do anything.
   *    exp is all 0s:  we have a denormalized value.  Multiply the fractional
   *                    part by 2.
   *    exp has 1s and 0s:  add 1 to the exponent's place to multiply the 
   *                        number by 2.
   */
  int exponentMask = 0xFF << 23;
  int exponent = uf & exponentMask;
  int oneInExponentPlace = 0x1 << 23;
  int signBitMask = (0x1 << 31);
  int signBit = uf & signBitMask;
  int significandMask = ~(signBitMask + exponentMask);
  int significand = uf & significandMask;
  int finalAns = 0;

  if(uf == 0)
    return uf;

  if(exponent == exponentMask)
    return uf;

  if(exponent == 0){
    significand = significand * 2;
    finalAns = signBit + exponent + significand;
    return finalAns;
  }

  exponent = exponent + oneInExponentPlace;
  exponent = exponent & exponentMask;

  finalAns = signBit + exponent + significand;
  return finalAns;
}
