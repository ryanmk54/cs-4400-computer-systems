/*
 * tsh - A tiny shell program with job control
 *
 * @author Ryan Kingston
 * @cade_login_id rykingst
 * @UID U0782618
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

/* Misc manifest constants */
#define MAXLINE    1024   /* max line size */
#define MAXARGS     128   /* max args on a command line */
#define MAXJOBS      16   /* max jobs at any point in time */
#define MAXJID    1<<16   /* max job ID */

/* Job states */
#define UNDEF 0 /* undefined */
#define FG 1    /* running in foreground */
#define BG 2    /* running in background */
#define ST 3    /* stopped */

/*
 * Jobs states: FG (foreground), BG (background), ST (stopped)
 * Job state transitions and enabling actions:
 *     FG -> ST  : ctrl-z
 *     ST -> FG  : fg command
 *     ST -> BG  : bg command
 *     BG -> FG  : fg command
 * At most 1 job can be in the FG state.
 */

/* Global variables */
extern char **environ;      /* defined in libc */
char prompt[] = "tsh> ";    /* command line prompt (DO NOT CHANGE) */
int verbose = 0;            /* if true, print additional output */
int nextjid = 1;            /* next job ID to allocate */
char sbuf[MAXLINE];         /* for composing sprintf messages */

struct job_t {              /* The job struct */
	pid_t pid;              /* job PID */
	int jid;                /* job ID [1, 2, ...] */
	int state;              /* UNDEF, BG, FG, or ST */
	char cmdline[MAXLINE];  /* command line */
};
struct job_t jobs[MAXJOBS]; /* The job list */
/* End global variables */


/* Function prototypes */

/* Here are the functions that you will implement */
void eval(char *cmdline);
int builtin_cmd(char **argv);
void do_bgfg(char **argv);
void waitfg(pid_t pid);

void sigchld_handler(int sig);
void sigtstp_handler(int sig);
void sigint_handler(int sig);

/* Here are helper routines that we've provided for you */
int parseline(const char *cmdline, char **argv);
void sigquit_handler(int sig);

void clearjob(struct job_t *job);
void initjobs(struct job_t *jobs);
int maxjid(struct job_t *jobs); 
int addjob(struct job_t *jobs, pid_t pid, int state, char *cmdline);
int deletejob(struct job_t *jobs, pid_t pid);
pid_t fgpid(struct job_t *jobs);
struct job_t *getjobpid(struct job_t *jobs, pid_t pid);
struct job_t *getjobjid(struct job_t *jobs, int jid);
int pid2jid(pid_t pid); 
void listjobs(struct job_t *jobs);

/* Helper routines from CS_App */
pid_t Fork(void);
void Execve(const char *filename, char *const argv[], char *const envp[]);
void Sigprocmask(int how, const sigset_t *set, sigset_t *oldset);
void Sigemptyset(sigset_t *set);
void Sigaddset(sigset_t *set, int signum);
void Setpgid(pid_t pid, pid_t pgid);
void Kill(pid_t pid, int signum);

/* Included helper methods */
void usage(void);
void unix_error(char *msg);
void app_error(char *msg);
typedef void handler_t(int);
handler_t *Signal(int signum, handler_t *handler);

/*
 * main - The shell's main routine 
 */
int main(int argc, char **argv) 
{
	char c;
	char cmdline[MAXLINE];
	int emit_prompt = 1; /* emit prompt (default) */

	/* Redirect stderr to stdout (so that driver will get all output
	 * on the pipe connected to stdout) */
	dup2(1, 2);

	/* Parse the command line */
	while ((c = getopt(argc, argv, "hvp")) != EOF) {
		switch (c) {
		case 'h':             /* print help message */
			usage();
			break;
		case 'v':             /* emit additional diagnostic info */
			verbose = 1;
			break;
		case 'p':             /* don't print a prompt */
			emit_prompt = 0;  /* handy for automatic testing */
			break;
		default:
			usage();
		}
	}

	/* Install the signal handlers */
	/* These are the ones you will need to implement */
	Signal(SIGINT,  sigint_handler);   /* ctrl-c */
	Signal(SIGTSTP, sigtstp_handler);  /* ctrl-z */
	Signal(SIGCHLD, sigchld_handler);  /* Terminated or stopped child */

	/* This one provides a clean way to kill the shell */
	Signal(SIGQUIT, sigquit_handler);

	/* Initialize the job list */
	initjobs(jobs);

	/* Execute the shell's read/eval loop */
	while (1) {
		fflush(stdout);
		/* Read command line */
		if (emit_prompt) {
			printf("%s", prompt);
			fflush(stdout);
		}
		fflush(stdout);
		if ((fgets(cmdline, MAXLINE, stdin) == NULL) && ferror(stdin))
			app_error("fgets error");
		fflush(stdout);
		if (feof(stdin)) { /* End of file (ctrl-d) */
			//printf("hit eof\n");
			fflush(stdout);
			exit(0);
			printf("past exit");
			fflush(stdout);
		}
		fflush(stdout);

		/* Evaluate the command line */
		eval(cmdline);
		fflush(stdout);
		fflush(stdout);
	}

	exit(0); /* control never reaches here */
}

/*
 * eval - Evaluate the command line that the user has just typed in
 *
 * If the user has requested a built-in command (quit, jobs, bg or fg)
 * then execute it immediately. Otherwise, fork a child process and
 * run the job in the context of the child. If the job is running in
 * the foreground, wait for it to terminate and then return.  Note:
 * each child process must have a unique process group ID so that our
 * background children don't receive SIGINT (SIGTSTP) from the kernel
 * when we type ctrl-c (ctrl-z) at the keyboard.
 */
void eval(char *cmdline)
{
	/* Parse command line */
	char* argv[MAXARGS];
	int bg = parseline(cmdline, argv);
	if (argv[0] == NULL){
		return; /* Ignore empty lines */
	}

	/* Check for builtin and exeute */
	if(builtin_cmd(argv))
		return;

/* After this point, the command is not 
 * builtin
 * blank line
 */

	/* Block signals until we can add the job to the job list */
	sigset_t mask;
	Sigemptyset(&mask);
	Sigaddset(&mask, SIGINT);
	Sigaddset(&mask, SIGTSTP);
	Sigaddset(&mask, SIGCHLD);
	Sigprocmask(SIG_BLOCK, &mask, NULL);

	/* Create Child process */
	pid_t pid;
	if((pid = Fork()) == 0){
		/* Put pgid in its own process group */
		Setpgid(0,0);
		/* Child unblocks signals */
		Sigprocmask(SIG_UNBLOCK, &mask, NULL);

		/* Child runs user job */
		Execve(argv[0], argv, environ);
		return;
	}

	/* Set the job state in a variable so I only have to call addjob once */
	int job_state;
	if(bg)
		job_state = BG;
	else
		job_state = FG;

	/* Add the job, then unblock signals */
	addjob(jobs, pid, job_state, cmdline);
	Sigprocmask(SIG_UNBLOCK, &mask, NULL);

	if(bg){
		/* print out the job that is in the background */
		int jid = pid2jid(pid);
		printf("[%d] (%d) %s", jid, pid, cmdline);
		fflush(stdout);
	}
	else{ /* job is running in the foreground */
		/* Parent waits for foreground job to terminate */
		waitfg(pid);
		fflush(stdout);
	}

	return;
}

/*
 * parseline - Parse the command line and build the argv array.
 *
 * Characters enclosed in single quotes are treated as a single
 * argument.  Return true if the user has requested a BG job, false if
 * the user has requested a FG job.
 */
int parseline(const char *cmdline, char **argv) 
{
	static char array[MAXLINE]; /* holds local copy of command line */
	char *buf = array;          /* ptr that traverses command line */
	char *delim;                /* points to first space delimiter */
	int argc;                   /* number of args */
	int bg;                     /* background job? */

	strcpy(buf, cmdline);
	buf[strlen(buf)-1] = ' ';     /* replace trailing '\n' with space */
	while (*buf && (*buf == ' ')) /* ignore leading spaces */
		buf++;

	/* Build the argv list */
	argc = 0;
	if (*buf == '\'') {
		buf++;
		delim = strchr(buf, '\'');
	}
	else {
		delim = strchr(buf, ' ');
	}

	while (delim) {
		argv[argc++] = buf;
		*delim = '\0';
		buf = delim + 1;
		while (*buf && (*buf == ' ')) /* ignore spaces */
			buf++;

		if (*buf == '\'') {
			buf++;
			delim = strchr(buf, '\'');
		}
		else {
			delim = strchr(buf, ' ');
		}
	}
	argv[argc] = NULL;

	if (argc == 0)  /* ignore blank line */
		return 1;

	/* should the job run in the background? */
	if ((bg = (*argv[argc-1] == '&')) != 0) {
		argv[--argc] = NULL;
	}
	return bg;
}

/*
 * builtin_cmd - If the user has typed a built-in command then execute
 * it immediately.
 */
int builtin_cmd(char **argv) 
{
	/* Quit command */
	if(strcmp("quit", argv[0]) == 0){
		exit(0);
	}
	/* Jobs command */
	else if(strcmp("jobs", argv[0]) == 0){
		listjobs(jobs);
		return 1;
	}
	/* bg or fg command */
	else if(strcmp("bg", argv[0]) == 0 || strcmp("fg", argv[0]) == 0){
		do_bgfg(argv);
		return 1;
	}

	return 0;     /* not a builtin command */
}

/* 
 * do_bgfg - Execute the builtin bg and fg commands
 */
void do_bgfg(char **argv)
{
	char* command = argv[0];
	const char* bgfg_arg = argv[1];

	/* Verify we've been given a jid or pid */
	if(bgfg_arg == NULL){
		printf("%s command requires PID or %%jobid argument\n", command);
		return;
	}

	struct job_t* job;
	if(bgfg_arg[0] == '%'){ /* its a jid */
		/* parse the jid and make sure its a number */
		int jid = atoi(&bgfg_arg[1]);
		if(jid == 0){
			printf("%s: argument must be a PID or %%jobid\n", command);
		}
		/* Try to pull this job out of the job list */
		job = getjobjid(jobs, jid);
		/* Check if the job was in the job list */
		if(job == NULL){
			printf("%%%d: No such job\n", jid);
			return;
		}
	}
	else{ /* its a pid */
		/* Parse the pid and make sure its a number */
		int pid = atoi(bgfg_arg);
		if(pid == 0){
			printf("%s: argument must be a PID or %%jobid\n", command);
			return;
		}

		/* Try to get the job out of the job list */
		job = getjobpid(jobs, atoi(bgfg_arg));

		/* Check if the job was in the job list */
		if(job == NULL){
			printf("(%d): No such process\n", pid);
			return;
		}
	}


	/* if the job is a foreground process wait for it to finish */
	if(strcmp(command, "fg") == 0){
		job->state = FG;

		/* Tell the job to continue */
		Kill(job->pid, SIGCONT);

		waitfg(job->pid);
	}
	else{ /* the command is "bg". Let it run in the background*/
		job->state = BG;

		/* Tell the job to continue */
		Kill(job->pid, SIGCONT);
		printf("[%d] (%d) %s", job->jid, job->pid, job->cmdline);
	}
	return;
}

/* 
 * waitfg - Block until process pid is no longer the foreground process
 */
void waitfg(pid_t pid)
{
	/* Sleep until the fg pid is different than the pid they gave us
	 * or there is no longer a fg pid.
	 */
	while(fgpid(jobs) == pid && fgpid(jobs) != 0){
		sleep(1);
	}

	if(verbose)
		printf("waitfg: Proecess (%d) no longer the fg process\n", pid);

	return;
}

/*****************
 * Signal handlers
 *****************/

/*
 * sigchld_handler - The kernel sends a SIGCHLD to the shell whenever
 * a child job terminates (becomes a zombie), or stops because it
 * received a SIGSTOP or SIGTSTP signal. The handler reaps all
 * available zombie children, but doesn't wait for any other
 * currently running children to terminate.
 */
void sigchld_handler(int sig)
{
    if(verbose)
        printf("sigchld_handler: entering\n");

	/* Sigchld hanlder needs to cover 3 cases:
	 * child has terminated,
	 * interrupted by SIGINT,
	 * interrupted by SIGSTP
	 */

	int status = 0;
	pid_t pid;
	/* Keeping responding to the child's signals until there are no more 
	 * zombie children left to reap
	 */
	while((pid = waitpid(-1, &status, WNOHANG|WUNTRACED)) > 0){
		fflush(stdout);
		int jid = pid2jid(pid);
		if(WIFEXITED(status)){ /* child exited normally */
			if(deletejob(jobs, pid) && verbose)
				printf("sigchld_handler: Job [%d] (%d) deleted\n", jid, pid);
			if(verbose)
				printf("sigchld_handler: Job [%d] (%d) terminates OK (status %d)\n", jid, pid, WEXITSTATUS(status));
		}
		else if(WIFSIGNALED(status)){ /* uncaught ctrl+c */
			if(deletejob(jobs, pid) && verbose)
				printf("sigchld_handler: Job [%d] (%d) deleted\n", jid, pid);
            printf("Job [%d] (%d) terminated by signal %d\n", jid, pid, WTERMSIG(status));
		}
		else if(WIFSTOPPED(status)){ /* CTRL+Z */
			struct job_t* job = getjobpid(jobs, pid);
			job->state = ST;
			printf("Job [%d] (%d) stopped by signal %d\n", jid, pid, WSTOPSIG(status));
		}
		else
			printf("child %d terminated abnormally\n", pid);
	}

	// Check to make sure waitpid didn't finish with an error
	if(pid == -1 && errno != ECHILD){
		unix_error("waitpid error");
	}

	if(verbose)
		printf("sigchild_handler: exiting\n");

	return;
}

/*
 * sigint_handler - The kernel sends a SIGINT to the shell whenver the
 *  user types ctrl-c at the keyboard.  Catch it and send it along
 *  to the foreground job.
 */
void sigint_handler(int sig)
{
	if(verbose)
		printf("sigint_handler: entering\n");

	/* When I get a ctrl+c, forward it to all child processes. */
	pid_t fg_pid = fgpid(jobs);
	if(fg_pid){
		Kill(fg_pid, SIGINT);
	}

	if(verbose)
		printf("sigint_handler: exiting\n");

	return;
}

/*
 * sigtstp_handler - The kernel sends a SIGTSTP to the shell whenever
 * the user types ctrl-z at the keyboard. Catch it and suspend the
 * foreground job by sending it a SIGTSTP.
 */
void sigtstp_handler(int sig)
{
	if(verbose)
		printf("sigstp_handler: entering\n");

	/* Get the fg_pid and fg_jid */
	pid_t fg_job_pid = fgpid(jobs);
	int jid = pid2jid(fg_job_pid);

	/* if there is a foreground job, then send it the SIGTSTP signal */
	if(fg_job_pid){
		Kill(fg_job_pid, SIGTSTP);

		if(verbose)
			printf("sigstp_handler: Job [%d] (%d) stopped\n", jid, fg_job_pid);
	}

	if(verbose)
		printf("sigstp_handler: exiting\n");

	return;
}

/*********************
 * End signal handlers
 *********************/

/***********************************************
 * Helper routines that manipulate the job list
 **********************************************/

/* clearjob - Clear the entries in a job struct */
void clearjob(struct job_t *job) {
	job->pid = 0;
	job->jid = 0;
	job->state = UNDEF;
	job->cmdline[0] = '\0';
}

/* initjobs - Initialize the job list */
void initjobs(struct job_t *jobs) {
	int i;

	for (i = 0; i < MAXJOBS; i++)
		clearjob(&jobs[i]);
}

/* maxjid - Returns largest allocated job ID */
int maxjid(struct job_t *jobs) 
{
	int i, max=0;

	for (i = 0; i < MAXJOBS; i++)
		if (jobs[i].jid > max)
			max = jobs[i].jid;
	return max;
}

/* addjob - Add a job to the job list */
int addjob(struct job_t *jobs, pid_t pid, int state, char *cmdline) 
{
	int i;

	if (pid < 1)
		return 0;

	for (i = 0; i < MAXJOBS; i++) {
		if (jobs[i].pid == 0) {
			jobs[i].pid = pid;
			jobs[i].state = state;
			jobs[i].jid = nextjid++;
			if (nextjid > MAXJOBS)
				nextjid = 1;
			strcpy(jobs[i].cmdline, cmdline);
			if(verbose){
				printf("Added job [%d] %d %s\n", jobs[i].jid, jobs[i].pid, jobs[i].cmdline);
			}
			return 1;
		}
	}
	printf("Tried to create too many jobs\n");
	return 0;
}

/* deletejob - Delete a job whose PID=pid from the job list */
int deletejob(struct job_t *jobs, pid_t pid) 
{
	int i;

	if (pid < 1)
		return 0;

	for (i = 0; i < MAXJOBS; i++) {
		if (jobs[i].pid == pid) {
			clearjob(&jobs[i]);
			nextjid = maxjid(jobs)+1;
			return 1;
		}
	}
	return 0;
}

/* fgpid - Return PID of current foreground job, 0 if no such job */
pid_t fgpid(struct job_t *jobs) {
	int i;

	for (i = 0; i < MAXJOBS; i++)
		if (jobs[i].state == FG)
			return jobs[i].pid;
	return 0;
}

/* getjobpid    - Find a job (by PID) on the job list */
struct job_t *getjobpid(struct job_t *jobs, pid_t pid) {
	int i;

	if (pid < 1)
		return NULL;
	for (i = 0; i < MAXJOBS; i++)
		if (jobs[i].pid == pid)
			return &jobs[i];
	return NULL;
}

/* getjobjid    - Find a job (by JID) on the job list */
struct job_t *getjobjid(struct job_t *jobs, int jid) 
{
	int i;

	if (jid < 1)
		return NULL;
	for (i = 0; i < MAXJOBS; i++)
		if (jobs[i].jid == jid)
			return &jobs[i];
	return NULL;
}

/* pid2jid - Map process ID to job ID */
int pid2jid(pid_t pid)
{
	int i;

	if (pid < 1)
		return 0;
	for (i = 0; i < MAXJOBS; i++)
		if (jobs[i].pid == pid) {
			return jobs[i].jid;
		}
	return 0;
}

/* listjobs - Print the job list */
void listjobs(struct job_t *jobs)
{
	int i;

	for (i = 0; i < MAXJOBS; i++) {
		if (jobs[i].pid != 0) {
			printf("[%d] (%d) ", jobs[i].jid, jobs[i].pid);
			switch (jobs[i].state) {
				case BG: 
					printf("Running ");
					break;
				case FG: 
					printf("Foreground ");
					break;
				case ST: 
					printf("Stopped ");
					break;
				default:
					printf("listjobs: Internal error: job[%d].state=%d ", 
							i, jobs[i].state);
			}
			printf("%s", jobs[i].cmdline);
		}
	}
}
/******************************
 * end job list helper routines
 ******************************/


/***********************
 * Other helper routines
 ***********************/

/*
 * usage - print a help message
 */
void usage(void) 
{
    printf("Usage: shell [-hvp]\n");
    printf("     -h     print this message\n");
    printf("     -v     print additional diagnostic information\n");
    printf("     -p     do not emit a command prompt\n");
    exit(1);
}

/*
 * unix_error - unix-style error routine
 */
void unix_error(char *msg)
{
    fprintf(stdout, "%s: %s\n", msg, strerror(errno));
    exit(1);
}

/*
 * app_error - application-style error routine
 */
void app_error(char *msg)
{
	fprintf(stdout, "%s\n", msg);
	exit(1);
}

/*
 * Signal - wrapper for the sigaction function
 */
handler_t *Signal(int signum, handler_t *handler)
{
	struct sigaction action, old_action;

	action.sa_handler = handler;
	sigemptyset(&action.sa_mask); /* block sigs of type being handled */
	action.sa_flags = SA_RESTART; /* restart syscalls if possible */

	if (sigaction(signum, &action, &old_action) < 0)
		unix_error("Signal error");
	return (old_action.sa_handler);
}

/*
 * sigquit_handler - The driver program can gracefully terminate the
 *      child shell by sending it a SIGQUIT signal.
 */
void sigquit_handler(int sig)
{
    printf("Terminating after receipt of SIGQUIT signal\n");
    exit(1);
}

/*********************************************
 *    Wrapper methods Copied from csapp.c
 ********************************************/

pid_t Fork(void)
{
	pid_t pid;

	if ((pid = fork()) < 0)
		unix_error("Fork error");
	return pid;
}


void Execve(const char *filename, char *const argv[], char *const envp[]) 
{
	if (execve(filename, argv, envp) < 0){
		printf("%s: Command not found\n", argv[0]);
		fflush(stdout);
		exit(0);
	}
}


void Sigaddset(sigset_t *set, int signum)
{
	if (sigaddset(set, signum) < 0)
		unix_error("Sigaddset error");
	return;
}


void Sigemptyset(sigset_t *set)
{
	if (sigemptyset(set) < 0)
		unix_error("Sigemptyset error");
	return;
}


void Sigprocmask(int how, const sigset_t *set, sigset_t *oldset)
{
	if (sigprocmask(how, set, oldset) < 0)
		unix_error("Sigprocmask error");
	return;
}


void Kill(pid_t pid, int signum) 
{
	/* Switch pid to -pid because 
	 * the signal needs to go to all the proceses children as well
	 */
	if(kill(-pid, signum) < 0)
		unix_error("Kill error");
	return;
}


void Setpgid(pid_t pid, pid_t pgid) {
	int rc;

	if ((rc = setpgid(pid, pgid)) < 0)
		unix_error("Setpgid error");
	return;
}
