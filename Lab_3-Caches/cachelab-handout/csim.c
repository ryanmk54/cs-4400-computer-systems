/*
 * Author: Ryan Kingston
 * login: rykingst
 * UID: U0782618
 */

#include <ctype.h>
#include <getopt.h>
#include <math.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "cachelab.h"


void printHelp(char* programName){
  printf("Usage: %s [-hv] -s <num> -E <num> -b <num> -t <file> \n"
          "Options:                                            \n"
          "  -h         Print this help message.               \n"
          "  -v         Optional verbose flag.                 \n"
          "  -s <num>   Number of set index bits.              \n"
          "  -E <num>   Number lines per set.                  \n"
          "  -b <num>   Number of block offset bits.           \n"
          "  -t <file>  Trace file                             \n"
          "                                                    \n"
          "Examples:                                           \n"
          "  linux>  %s -s 4 -E 1 -b 4 -t traces/yi.trace      \n"
          "  linux>  %s -v -s 8 -E 2 -b 4 -t traces/yi.trace   \n", 
          programName, programName, programName);
}


unsigned long long createRightBitMask(int numBits){
  unsigned mask = 0;
  for(int i = 0; i < numBits; i++){
    mask <<= 1;
    mask |= 1;
  }
  return mask;
}


int main(int argc, char **argv){
  bool verboseOn = false;
  bool printHelpOn = false;
  char* nameValgrindTrace = "";
  int numHits = 0;
  int numMisses = 0;
  int numEvictions = 0;
  int numColumns = 0;
  int numBlockBits = 0;
  int numSetBits = 0;

  // begin parsing getopts taken from 
  // http://www.gnu.org/software/libc/manual/html_node/Example-of-Getopt.html
  // getops required vars
  int optChar;
  opterr = 0;
  while ((optChar = getopt(argc, argv, "hvs:E:b:t:")) != -1)
    switch (optChar){
      case 'v':
        verboseOn = true;
        break;
      case 'h':
        printHelpOn = true;
        break;
      case 's':
        numSetBits = atoi(optarg);
        break;
      case 'E':
        numColumns = atoi(optarg);
        break;
      case 'b':
        numBlockBits = atoi(optarg);
        break;
      case 't':
        nameValgrindTrace = optarg;
        break;
      case '?':
        if (optopt == 's' || optopt == 'E' || optopt == 'b' || optopt == 't')
          fprintf(stderr, "%s: Option requires an argument -- '%c'\n", argv[0], 
                  optopt);
        else if(isprint(optopt))
          fprintf(stderr, "%s Invalid option `-%c'.\n", argv[0], optopt);
        printHelp(argv[0]);
        return 0;
      default:
        printf("default case %c - %s\n", optopt, optarg);
      }
  // end parsing getopts

  if(numSetBits == 0   || numColumns == 0 || 
     numBlockBits == 0 || strcmp(nameValgrindTrace,"") == 0){
    fprintf(stderr, "%s: Missing required command line argument\n", argv[0]);
    printHelp(argv[0]);
    return 0;
  }

  if(printHelpOn){
    printHelp(argv[0]);
    return 0;
  }

  int numTagBits = 64 - numSetBits - numBlockBits;
  int numSets = pow(2, numSetBits);

  FILE* traceFile =  fopen(nameValgrindTrace, "r");
  if(traceFile == NULL){
    printf("unable to open file: %s\n", nameValgrindTrace);
    return 1;
  }

  // Struct to represent one line in the cache
  struct CacheLine{
    unsigned long long tag;
    int age;  // age of 0 means the the block is invalid
  };
  struct CacheLine cache[numSets][numColumns];

  // Initialize the cache to 0
  for(int i = 0; i < numSets; i++){
    for(int j = 0; j < numColumns; j++){
      cache[i][j].tag = 0;
      cache[i][j].age = 0;
    }
  }

  char line [20];
  while(fgets(line, 100, traceFile) != NULL){
    // Remove the newline to make formatting nicer
    int strLength = strlen(line);
    line[strLength - 1] = 0;

    if(verboseOn){
      printf("%s ", line);
    }

    // Get the instruction type, address, set and tag 
    char instructionType[1];
    unsigned long long int* address = malloc(sizeof(int));
    sscanf(line, "%s %llx", instructionType, address);
    int set = (*address >> numBlockBits) & createRightBitMask(numSetBits);
    unsigned long long int tag = (*address >> (numBlockBits + numSetBits)) & 
                                 createRightBitMask(numTagBits);

    if(strcmp(instructionType, "I") == 0)
        continue;


    bool tagFound = false;
    int lruPosition = 0;
    for(int i = 0; i < numColumns; i++){
      if(cache[set][i].age == 0){
        lruPosition = i;
        break;
      }
      else{
        if(cache[set][i].tag == tag){
          tagFound = true;
          numHits++;
          lruPosition = i;
          if(verboseOn)
            printf("hit");
          break;
        }
        if(cache[set][i].age > cache[set][lruPosition].age)
          lruPosition = i;
      }
    } 
    if(!tagFound){
      if(cache[set][lruPosition].age != 0){
        numEvictions++;
        if(verboseOn){
          printf("eviction ");
        }
      }
      numMisses++;
      if(verboseOn){
        printf("miss ");
      }
    }

    // Add the count of lru
    // the block with the highest age is the lru block that should be replaced
    for(int i = 0; i < numColumns; i++){
      if(i == lruPosition)
        continue;
      if(cache[set][i].age == 0)
        continue;
      cache[set][i].age++;
    }

    cache[set][lruPosition].age = 1;
    cache[set][lruPosition].tag = tag;

    if(strcmp(instructionType, "M") == 0){
      numHits++;
      if(verboseOn)
        printf("hit ");
    }
    printf("\n");
  }
  fclose(traceFile);

  printSummary(numHits, numMisses, numEvictions);
  return 0;
}
