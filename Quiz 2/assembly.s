loop:
    movq    %rsi, %rcx
    movl     $1, %edx
    movl     $0, %eax

.L3:
    movq   %rdx, %r8
    andq     %rdi, %r8
    orq        %r8, %rax
    salq       %cl, %rdx
    testq    %rdx, %rdx
    jne        .L3
    rep; ret
